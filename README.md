## 1. Download
With git `git clone https://gitlab.com/studioneviem_tv/sha-token-generator`
<br />
With wget `https://gitlab.com/studioneviem_tv/sha-token-generator`
## 2. Install
#### Python
With apt `apt install python3`
<br />
With pacman `pacman -S python3`
<br />
With dnf `dnf install python3`
#### Hashlib
With pip `pip install hashlib`
## 3. Run
With python `python3 main.py`