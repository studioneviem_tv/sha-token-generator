"""
SHA Generator in python3
by root-user
"""

# Imports and that stuff
import hashlib as hashlib;
userChoose = 0; # 1 = sha256 # 2 = sha512
inputString = 0; # String input from user
outputString = 0; # String output to user
mainSwitch = True # True = on(default) # False = off

if mainSwitch == True:

	# Select
	print('Welcome in SHA Token Generator');
	print('Select your type of sha:');
	print('1) sha256');
	print('2) sha512');
	userChoose = int(input('> ')); # Userinput for choose between sha256 and sha512
	print(' '); # Print empty line

	# sha256
	if userChoose == 1:
		print('sha256 selected');
		print('Insert your text:');
		inputString = str(input('> ')); # Input string from user
		outputString = hashlib.sha256(str.encode(inputString)).hexdigest(); # Output string for user in sha256
		print('Your sha256 token is:');
		print(outputString);
		exit('Program completed successfully');

	# sha512
	elif userChoose == 2:
		print('sha512 selected');
		print('Insert your text:');
		inputString = str(input('> ')); # Input string from user
		outputString = hashlib.sha512(str.encode(inputString)).hexdigest(); # Output string for user in sha512
		print('Your sha512 token is:');
		print(outputString);
		exit('Program completed successfully');

	# Wrong sha selected
	else:
		print('We have a problem here');
		print('Contact Administrator');
		print('error.sha.invalidSelect'); # Error message
		exit(' ');

# Switch off
elif mainSwitch == False:
	print('Switch is off'); # You can change it on line 11
	exit(' ');

# Switch error
else:
	print('We have a problem here');
	print('Contact Administrator');
	print('error.switch.invalidSelect'); # Error message
	exit(' ');

# End of code